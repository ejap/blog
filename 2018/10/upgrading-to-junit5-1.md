# 26/10/2018

## Upgrading to JUnit 5 - Part 1

__JUnit 5__ have many new features that make writing unit tests more enjoyable.

Surprisingly, it is easy to upgrade our Java App to use JUnit 5 without making any changes to existing JUnit 4 unit tests.

a. Add JUnit 5 API (__junit-jupiter-api__) to pom.xml. Keep the existing JUnit 4 API if found in your pom.xml (if not exists, JUnit 4 most likely included from another artifacts)

```xml
<!-- DO NOT DELETE ME IF EXISTS :-) -->
<dependency>
    <groupId>junit</groupId>
    <artifactId>junit</artifactId>
    <version>4.12</version>
    <scope>test</scope>
</dependency>
 
<!-- ADD ME -->
<dependency>
    <groupId>org.junit.jupiter</groupId>
    <artifactId>junit-jupiter-api</artifactId>
    <version>5.3.1</version>
    <scope>test</scope>
</dependency>
```

b. Write new unit tests using JUnit 5 API. IntelliJ support creating JUnit 5 test once you add JUnit 5 API.

![Create Test](assets_upgrading-to-junit5-1/create-junit5-test.png "Create Test")

```java
import org.junit.jupiter.api.Test;
 
class PersonLogicTest {
 
    @Test
    void testGetProviderBySubscription() {
        //JUnit 5 assertion methods is from class org.junit.jupiter.api.Assertions
        Assertions.assertEquals(...
    }
```

c. Run the test. Latest __IntelliJ__ support JUnit 5 and all the tests from both JUnit 4 and JUnit 5 will be discovered and run. They will be placed in separated root branch.

![Run Test](assets_upgrading-to-junit5-1/run_tests.png "Run Test")

d. __First problem encountered__: When running _mvn test_, notice that only JUnit 4 tests will be discovered. This is because the default surefire plugin provider only recognize JUnit 4 unit tests.

This can be fixed by adding a JUnit 5 surefire provider to the Maven surefire plugin.

```xml
<plugin>
    <artifactId>maven-surefire-plugin</artifactId>
    <version>2.22.1</version>
    <dependencies>
        <dependency>
            <groupId>org.junit.platform</groupId>
            <artifactId>junit-platform-surefire-provider</artifactId>
            <version>1.3.1</version>
        </dependency>
    </dependencies>
</plugin>
```

e. __Second problem encountered__: Rerun _mvn test_. You will get the following error:

```
Cannot create Launcher without at least one TestEngine; consider adding an engine implementation JAR to the classpath
```

This can be fixed by adding a TestEngine as recommended by the error message. Add __junit-jupiter-engine__ to the dependency for surefire plugin:

```xml
<plugin>
    <artifactId>maven-surefire-plugin</artifactId>
...
        <dependency>
            <groupId>org.junit.platform</groupId>
            <artifactId>junit-platform-surefire-provider</artifactId>
            <version>1.3.1</version>
        </dependency>
 
        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter-engine</artifactId>
            <version>5.3.1</version>
        </dependency>
...
```

Run __mvn test__ again. Now the error goes away.

f. __Third Problem encountered__: now Maven will discover JUnit 5 unit tests, but no longer discover older (JUnit 4) tests. The fix is to add a test engine that support JUnit 4 (__junit-vintage-engine__).

```xml
<!-- Final pom.xml -->
<plugin>
    <artifactId>maven-surefire-plugin</artifactId>
    <version>2.22.1</version>
    <dependencies>
      <dependencies>
        <dependency>
            <groupId>org.junit.platform</groupId>
            <artifactId>junit-platform-surefire-provider</artifactId>
            <version>1.3.1</version>
        </dependency>
 
        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter-engine</artifactId>
            <version>5.3.1</version>
        </dependency>
 
        <dependency>
            <groupId>org.junit.vintage</groupId>
            <artifactId>junit-vintage-engine</artifactId>
            <version>5.3.1</version>
        </dependency>
```

Run mvn test again. Now all tests will be discovered by Maven.
 
Future Posts:
- Better unit testing
  - Exceptions checking in test body (JUnit 4 exceptions checking at method level)
  - Timeout (e.g. fail test if takes too long)

- New features
  - More assertions
  - Nested test
  - Dynamic test (create test case at runtime ???)
  - Tag your test (i.e. you can tag tests as Development only so it won't run on Bamboo for example)

- Mockito extension