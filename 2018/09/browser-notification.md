# 12/09/2018

## Browser Notification

One of the feature of dashboard style web application that is considered a "must have"
is notification. As users can't be expected to have the dashboard page open all the
time, there should be a way to show notification.

On Windows, before Windows 10 the most common way to show notification is via 
balloon tip. Every application must code it own notification using Windows API that
called directly or wrapped by libraries.

![Balloon](assets_browser-notification/baloon-tips.png "Balloon")


As far as I know, browsers at that time doesn't have a way or standardized way to
show notification until the introduction of [ServiceWorkerRegistration.showNotification()](https://developer.mozilla.org/en-US/docs/Web/API/ServiceWorkerRegistration/showNotification)
which is until today still considered "experimental" even though most modern 
browsers support it. A great page to test notification is this page: 
[https://web-push-book.gauntface.com/demos/notification-examples/](https://web-push-book.gauntface.com/demos/notification-examples/)

As with any feature, this feature is bound to be misused by "virus" or "adware"
to push ads to user. So every web page that wish to display notification need
to get approval from user. Chrome also provide a setting to block all notification.

![Ask](assets_browser-notification/ask-notification.png "Ask")

The settings can be found on [chrome://settings/content/notifications](chrome://settings/content/notifications)

![Settings](assets_browser-notification/chrome-settings-notifications.png "Settings")

As this feature gain more use, Chrome has recently make the notification to be
more tightly integrated with the underlying OSes notification system. This will
be the Windows Action Centre on Windows 10 as announced via this tweet: 
[https://twitter.com/beverloo/status/1027258639688654854](https://twitter.com/beverloo/status/1027258639688654854). Notification will also
now appeared on Lock screen if enabled. This is a big win for Google Calendar
users where previously only email notification from native application such as
Outlook will show notifications on Lock screen.

There are also experimental feature flags that as developer you can enable/disable
to test a certain features. There are about 3 that are related to notification.
For example, you can disable the native notification integration to see how
notification looks like in older browser.

![Flags](assets_browser-notification/chrome-flags.png "Flags")

### References

- [https://developers.google.com/web/fundamentals/push-notifications/display-a-notification](https://developers.google.com/web/fundamentals/push-notifications/display-a-notification)
- [https://developer.mozilla.org/en-US/docs/Web/API/ServiceWorkerRegistration/showNotification](https://developer.mozilla.org/en-US/docs/Web/API/ServiceWorkerRegistration/showNotification)
