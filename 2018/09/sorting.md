# 21/09/2018

## Sorting

### Simple Sorting

This post will use the following data object as example:

```java
public class Order {

    public enum StatusEnum {
        Appointment,
        InProgress,
        Saved
    }

    private String orderNumber;
    private LocalDate lastModifiedDate;
    private StatusEnum status;
    private int labour;
    ...
    //NOTE: setter/getter/etc here
```

And the object __List\<Order\> orders__ populated as:

| orderNumber | lastModifiedDate | status | labour |
| ----------- | ---------------- | ------ | ------ |
| RO001 | 20/09/2018 | Appointment | 10 |
| RO003 | 01/09/2018 | Appointment | 30 |
| RO004 | 15/09/2018 | InProgress | 15 |
| RO002 | 28/09/2018 | Saved | 2 |

Let's try to sort orders:

```java
orders.stream()
    .sorted()
    .collect(Collectors.toList()))
```

The following error was thrown:
```
Exception in thread "main" java.lang.ClassCastException: ... .Order cannot be cast to java.lang.Comparable
```

To use __.sorted()__ we have to implement Comparable interface and override the __compareTo()__ method:

```java
public class Order implements Comparable<Order> {

    @Override
    public int compareTo(Order order) {
        /* NOTE:
            - write code here to compare current object (this) and object to compare with
            - return one of -1, 0, or 1
            - for more details, see: https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet 
            - the easiest is to use each member object's compareTo() method
        */

        //to sort object by orderNumber:
        return this.orderNumber.compareTo(order.orderNumber);
    }
```

Running
```java
orders.stream()
    .sorted()
    .collect(Collectors.toList()))
```

| orderNumber | lastModifiedDate | status | labour |
| ----------- | ---------------- | ------ | ------ |
| RO001 | 20/09/2018 | Appointment | 10 |
| RO002 | 28/09/2018 | Saved | 2 |
| RO003 | 01/09/2018 | Appointment | 30 |
| RO004 | 15/09/2018 | InProgress | 15 |

To sort by orderNumber in reverse, just add negative operator:

```java

    @Override
    public int compareTo(Order order) {
        return -this.orderNumber.compareTo(order.orderNumber);
    }
```

| orderNumber | lastModifiedDate | status | labour |
| ----------- | ---------------- | ------ | ------ |
| RO004 | 15/09/2018 | InProgress | 15 |
| RO003 | 01/09/2018 | Appointment | 30 |
| RO002 | 28/09/2018 | Saved | 2 |
| RO001 | 20/09/2018 | Appointment | 10 |

### Sorting by different field depending on need?

You can leave the compareTo() as is and use some of the helper methods provided by Java Streams API.

Let's say you want to sort orders by __lastModifiedDate__

```java
orders.stream()
    .sorted(Comparator.comparing(
        order -> order.getLastModifiedDate()))
    .collect(Collectors.toList());        
```

| orderNumber | lastModifiedDate | status | labour |
| ----------- | ---------------- | ------ | ------ |
| RO003 | 01/09/2018 | Appointment | 30 |
| RO004 | 15/09/2018 | InProgress | 15 |
| RO001 | 20/09/2018 | Appointment | 10 |
| RO002 | 28/09/2018 | Saved | 2 |

Calling .sorted() without any parameter will use the sort logic implemented in the __compareTo()__ method

How to sort by __lastModifiedDate__ in reverse (the more recent one first)

```java
orders.stream()
    .sorted(Comparator
        .comparing(Order::getLastModifiedDate)
        .reversed()
    )
    .collect(Collectors.toList())
```

| orderNumber | lastModifiedDate | status | labour |
| ----------- | ---------------- | ------ | ------ |
| RO002 | 28/09/2018 | Saved | 2 |
| RO001 | 20/09/2018 | Appointment | 10 |
| RO004 | 15/09/2018 | InProgress | 15 |
| RO003 | 01/09/2018 | Appointment | 30 |

### Sort by multiple fields

Method chaining is one of the joy of working with Streams API and it is the same if you want to sort by multiple fields

Let's sort by __status__  and then by __lastModifiedDate__ 

```java
orders.stream()
    .sorted(
        Comparator
            .comparing(Order::getStatus)
            .thenComparing(Order::getLastModifiedDate))
    .collect(Collectors.toList())
```

| orderNumber | lastModifiedDate | status | labour |
| ----------- | ---------------- | ------ | ------ |
| RO003 | 01/09/2018 | Appointment | 30 |
| RO001 | 20/09/2018 | Appointment | 10 |
| RO004 | 15/09/2018 | InProgress | 15 |
| RO002 | 28/09/2018 | Saved | 2 |

You can also add __.reverse()__ for the fields that you want to sort in descending order.
