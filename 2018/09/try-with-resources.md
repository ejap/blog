# 24/09/2018

## Code Review Series: Try-With-Resources

In a recent code review, I come across an existing code as shown below:

```java
fileName = extractFileNameFromHeader(CDHeader);
InputStream inputStream= part.getInputStream();
fileName = new Date().getTime() + "_" + fileName;
out = new FileOutputStream(temporaryUploadPath + "\\" + fileName);
int read=0;
bytes = new byte[1024];
while((read = inputStream.read(bytes))!= -1){
    out.write(bytes, 0, read);
}
inputStream.close();
out.flush();
out.close();
```

The code is working fine, but it might be better written in this way:

```java
fileName = extractFileNameFromHeader(CDHeader);
fileName = new Date().getTime() + "_" + fileName;
try (InputStream inputStream= part.getInputStream();
     OutputStream out = new FileOutputStream(temporaryUploadPath + "\\" + fileName)) {
    int read=0;
    bytes = new byte[1024];
    while((read = inputStream.read(bytes))!= -1){
        out.write(bytes, 0, read);
    }
}
```

The benefit is that we don't have to remember to call __.flush()__ and __.close()__ after we are done with it.

 
If we want to make it "better", we can and it is actually quite easy to write classes that can utilize try-with-resources. You only need to implement interface __@AutoCloseable__ and override/implement the method __close()__ where you can place the resource's __.close()__ or __.flush()__ method calls in it and __.close()__ will be automatically called at the end of try-with-resources block.


Example is this existing code:

```java
InputStream in = manager.downloadFileAsStream(...);
...
List<T> dataLines = csvFileReader.readAll(in, fieldsToImport);
Integer linesRead = dataLines.size();
...
```

The code is fine except that the content of the "resource" will be read into memory:
```java
List<T> dataLines = csvFileReader.readAll(in, fieldsToImport);
```
 
To make it even better, we can do:

```java
try (CSVFileReaderThatSupportReadOneLine csvFileReader = new CSVFileReaderThatSupportReadOneLine(manager.downloadFileAsStream(...)) {
 
    ....
    T dataLine = csvFileReader.readOneLine(fieldsToImport);
    ....
}
```

```java
class CSVFileReaderThatSupportReadOneLine implements AutoCloseable {
 
    public CSVFileReaderThatSupportReadOneLine(InputStream inputStream) {
        this.inputStream = inputStream;
        this.outputStream = new FileOutputStream(...)
    }
 
    @Override
    public void close() throws Exception {
        inputStream.close();
        outputStream.close();
    }
}
```
