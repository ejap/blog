# 14/09/2018

## Bitwise Operation

Bitwise operation is one the Computer Science 101 topic that you
have to know because it is the fundamental of how a computer works.

In Java, bitwise operation can be used for the following types:

- long
- int
- short
- char
- byte

You will mostly likely use it with the byte (8 bits) types.

Example of a byte varible:

```
byte flag = 0b1011_0101;
```

And these are the list of bitwise operators

| Operator | Description           |
| -------- | --------------------- |
| ~        | complement operator   |
| <<       | shift left operator   |
| >>       | shift right operator  |
| &        | and operator          |
| ^        | exclusive or operator |
| |        | inclusive or operator |

Examples:

| Var 1 | Op | Var 2 | Output |
| ----- | -- | ----- | ------ |
| 0011  | ~  | -     | 1100   |
| 0011  | << | -     | 0100   |
| 0011  | >> | -     | 0001   |
| 0011  | &  | 1010  | 0010   |
| 0011  | ^  | 1010  | 0110   |
| 0011  | &#124;  | 1010  | 1010 |

### References

- [https://docs.oracle.com/javase/tutorial/java/nutsandbolts/op3.html](https://docs.oracle.com/javase/tutorial/java/nutsandbolts/op3.html)
