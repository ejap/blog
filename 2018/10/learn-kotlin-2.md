# 25/10/2018

## Learning Kotlin - Part 2

See part one of the Learning Kotlin here: [Learning Kotlin - Part 1](learn-kotlin-1.md)
 
If mixing up Java and Kotlin code in a project is not for you, you can write a "Java/JVM" library using Kotlin that can be used from Java. The user of your library can simply add the <dependency> to the project to start using the Kotlin library.

The easiest way to create a Maven/Kotlin project is by using the maven's archetype:generate command. The archetype to use is __org.jetbrains.kotlin:kotlin-archetype-jvm__.

![Maven Archetype](assets_learn-kotlin-2/maven-archetype.png "Maven Archetype")
 
Click next to specify the project's group and artifact id. For this exercise, I will call it:

```xml
<groupId>au.id.triagedevs</groupId>
<artifactId>euler-ssm-proxy</artifactId>
<version>1.0-SNAPSHOT</version>
```

Then add some "library" code, I will add a new Kotlin file (i.e. Person.kt), and add a new class called PersonLogic and a new public method call getNewPersonId().

```kotlin
//Person.kt
package au.id.triagedevs
 
import java.util.*
 
class PersonLogic {
 
    fun getNewPersonId(): String {
        return UUID.randomUUID().toString()
    }
}
```

Do a mvn install to build the project and copy the library jar to local maven repository.
 
In another (Java) project, I can use the library by adding the dependency in pom.xml

```xml
<dependencies>
   <depedency>
      <groupId>au.id.triagedevs</groupId>
      <artifactId>euler-ssm-proxy</artifactId>
      <version>1.0-SNAPSHOT</version>
...
```

If Auto-Import enabled, I can start using it in my Java code.

```java
PersonLogic personLogic = new PersonLogic();
String newId = personLogic.getNewPersonId();
```

I will add another method to show the Extension Functions feature of Kotlin:

```java
package au.id.triagedevs
 
class Part(val partNumber: String) {
 
    private fun String.getFirst3Char(): String {
        return this.substring(0, 3)
    }
 
    fun getPartPrefix(): String {
        return "P" + partNumber.getFirst3Char() + "-"
    }
}
```

In our Java project, we can use it as follows:

```java
Part part = new Part("12345678");
String partNumber = part.getPartNumber();   //we get getter for free
String partPrefix = part.getPartPrefix();
```
