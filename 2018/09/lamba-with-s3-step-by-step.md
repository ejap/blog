# 27/09/2018

## AWS Lambda - With S3 - Step by Step

Let's dive right into it :-)

### 1. Pre-requisites

Sign up for AWS account. You will need a credit card and phone number for AWS to call for verification.

Most services are free before you hit a certain limit. For example, the first 1 Million AWS Lambda requests are free.

Spend an hour or two every day to go through AWS services and settings,especially the core services such as:
- IAM
- CloudWatch
- The account dashboards (Billing and Security Credentials)

Setup MFA (Multi Factor Authentication) to familiarize with it. You "must" use it if you are doing Production setup/configuration. You can use a slew of authenticator apps as __Virtual MFA Device". 

The one that I use is "Microsoft Authenticator" on my Android device as I already use it for other purposes.

### 2. Creating Lambda Function

a. Navigate to __AWS Lambda__ console and click the button __Create function__

![Create Lambda Function](assets_lambda-with-s3-step-by-step/2-create-lambda.jpg "Create Lambda Function")

b. Select __Author from scratch__ box and specify the followings:

| Field | Value |
| - | - |
| Name | _name of your function_ (can be anything) |
| Runtime | select __Java 8__ |
| Role | select __Create a new role from one or more templates.__ |
| Role name | _name of your role_ (can be anything) - as the information dialog suggest, you can modify it later in the __AWS IAM__ console |
| Policy templates | _leave it blank_ |

Once done, click __Create function__ button

![Create Lambda Function](assets_lambda-with-s3-step-by-step/2-create-lambda-authoring.jpg "Create Lambda Function")

c. It takes a few seconds, and if all well a new Lambda function is created.

We need to provide the code that we are going to write in the next section so let leave this section here.

![Create Lambda Function](assets_lambda-with-s3-step-by-step/2-create-lambda-created.jpg "Create Lambda Function")

### 3. Write the function code

For __Java 8__ runtime, we need to write the code on our development machine and then upload it. This section assume that you are a Java developer with all the following tools installed and ready to use:
- IntelliJ
- Maven 
- Java 8 SDK

a. In IntelliJ, create a new blank Maven project. Once created, open pom.xml and add the following dependencies:

```
<dependency>
    <groupId>com.amazonaws</groupId>
    <artifactId>aws-lambda-java-core</artifactId>
    <version>1.2.0</version>
</dependency>

<dependency>
    <groupId>com.amazonaws</groupId>
    <artifactId>aws-lambda-java-events</artifactId>
    <version>1.3.0</version>
</dependency>
```

The versions used here are the latest 1.x.x versions. The libraries are ever changing (methods were renamed or made obsolete, etc) so you might need to modify the code when using newer versions of the library.

b. Add the following build plugin to build a _fat jar_ (that include all required dependencies) when you do _mvn package_

```
<build>
    <plugins>
        <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-shade-plugin</artifactId>
            <version>2.4.3</version>
            <configuration>
                <createDependencyReducedPom>false</createDependencyReducedPom>
            </configuration>
            <executions>
                <execution>
                    <phase>package</phase>
                    <goals>
                        <goal>shade</goal>
                    </goals>
                </execution>
            </executions>
        </plugin>
    </plugins>
</build>
```

c. Create a new handler class as shown below. You will need to copy the package name, class name, and the handler method name (_handleRequest_) to your Lambda function.

```
package id.ejap;
...
public class DataFileProcessorHandler implements RequestHandler<S3Event, Boolean> {
    public Boolean handleRequest(S3Event s3Event, Context context) {
    
        //get logger (CloudWatch log)
        LambdaLogger logger = context.getLogger();
        
        //get the events (we only want S3 events)
        for (S3EventNotification.S3EventNotificationRecord record : s3Event.getRecords()) {
            
            //get the event details (we want bucket name and key/filename)
            S3EventNotification.S3Entity entity = record.getS3();
            String bucketName = entity.getBucket().getName();
            String objectKey = entity.getObject().getKey();

            //for now, we just log the bucket and filename
            logger.log(String.format("bucket=%s, object=%s", bucketName, objectKey));

            result = true;
        }
        return result;
    }
}
```

d. do a _mvn package_ to build the jar file.

### 4. Upload the code

Open __AWS S3__ console and create a new bucket. Give it a name like _ejap-app-lambda_ and upload the fat jar file created above.

![Upload Code](assets_lambda-with-s3-step-by-step/4-upload-jar.jpg "Upload Code")

Once uploaded, copy the link to the jar file which we will need to use in the next section.

### 5. Configure Lambda

a. Back to __AWS Lambda__ console, in the _Function code_ section, set the following:

| Field           | Value                            |
| --------------- | -------------------------------- | 
| Code entry type | __Upload a file from Amazon S3__ |
| Amazon S3 link URL | https://s3-ap-southeast-2.amazonaws.com/ejap-app-lambda/aws-lambda-processor-1.0-SNAPSHOT.jar |
| Handler | __id.ejap.DataFileProcessorHandler::handleRequest__ |

- The format for Handler is _\<package name>.\<class name>:\<method name>_

Once done, click __Save__ button.

![Configure Lambda](assets_lambda-with-s3-step-by-step/5-configure-lambda.jpg "Configure Lambda")

b. We can test the function with the __Test__ button next to the Save button, but first we need to configure the test event. Test event is like our mock data.

![Configure Lambda](assets_lambda-with-s3-step-by-step/5-configure-lambda-test.jpg "Configure Lambda")

c. In the test event, choose __S3 Put__ from the event template and give the test event a name. Our function will pick up this event and will read the bucket name and key (filename). So note it down. Click __Create__ button to create the test event.

![Configure Lambda](assets_lambda-with-s3-step-by-step/5-configure-lambda-test-event.jpg "Configure Lambda")

d. Back in the function, click the __Test__ button with the newly created test event selected. If all well, you will get success message with the  log message (bucket name and filename). You can also click the __(logs)__ link which will bring you to __AWS CloudWatch__ console

![Configure Lambda](assets_lambda-with-s3-step-by-step/5-configure-lambda-test-success.jpg "Configure Lambda")

### 6. Running function when new file uploaded (S3 Put event)

Now we need to call the lambda function when new file is uploaded.

a. Open __AWS S3__ console and create a new bucket. Give it a name like _ejap-data-lambda_ and select all the defaults.

b. Once created, open it's properties and click __Events__ box

![S3 Event](assets_lambda-with-s3-step-by-step/6-s3event.jpg "S3 Event")

c. Click __Add notification__ link

![S3 Event](assets_lambda-with-s3-step-by-step/6-s3event-new.jpg "S3 Event")

d. Speficy the followings:

| Field | Value |
| ----- | ----- |
| Name | _any name is fine_ |
| Events | tick __Put__ checkbox |
| Suffix | __.txt__ (or any other filter you wish) |
| Send to | __Lambda Function__ |
| Lambda | the name of the lambda function (e.g. __newDataFileProcessor__) |


![S3 Event](assets_lambda-with-s3-step-by-step/6-s3event-new-create.jpg "S3 Event")

Once done, click the __Save__ button.

e. Upload a file with extension .txt to the bucket. Open __AWS CloudWatch__
console in another tab to monitor if the function is executed.

f. In a few seconds, if all goes well you will notice new entries in the __AWS CloudWatch__ console. But most likely, you will be an exception:

![S3 Event](assets_lambda-with-s3-step-by-step/6-s3event-exception.jpg "S3 Event")

This is because when we create our role, it only give us the permission to run Lambda function and write to CloudWatch log.

g. Open __AWS IAM__ console and open the role that we created earlier (e.g. _new-data-file-process-role_)

![S3 Event](assets_lambda-with-s3-step-by-step/6-s3event-role.jpg "S3 Event")

g. Click __Attach Policies__ and filter by S3. Check the checkbox next to __AmazonS3ReadOnlyAccess__ policy. And click __Attach Policy__ button.

![S3 Event](assets_lambda-with-s3-step-by-step/6-s3event-role-s3.jpg "S3 Event")

h. Once attached, our role will look like this and back in __AWS S3__ console, upload another .txt file. This time, in the __AWS CloudWatch__ log, the bucket name and filename will be logged/printed.

![S3 Event](assets_lambda-with-s3-step-by-step/6-s3event-role-final.jpg "S3 Event")


### 7. What is next?

- In this post, we look at S3 event source. There are other event source that is more suitable for your use case. Most common event source beside S3 are _AWS SQS__ and __AWS DynamoDB__. More details can be found here [lambda-getting-started.md](lambda-getting-started.md)

- You will want to run another Lambda function or choose from a set of Lambda functions to run depending on the result. You can create a workflow or state machine using the __AWS Step Functions__.

- In real life scenario, you will want to run, test, and debug it locally before testing it on AWS. You also want to script the deployment or perform a CI/CD of your Lambda functions. One of the tool to help to achieve that is [https://github.com/awslabs/aws-sam-cli](https://github.com/awslabs/aws-sam-cli) which I am going to look at next.

- For more serious workload or call the Lambda function from your frontend, I will look into [https://github.com/awslabs/aws-serverless-java-container](https://github.com/awslabs/aws-serverless-java-container) to write the Lambda function using Spring Boot.
