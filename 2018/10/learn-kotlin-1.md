# 22/10/2018

## Learning Kotlin - Part 1

Although it is not new, Kotlin has gain traction lately due to an endorsement by Google as one of the official Android development language ([https://developer.android.com/kotlin/index.html](https://developer.android.com/kotlin/index.html)) and also dedicated support in the Spring Boot framework ([https://spring.io/blog/2016/02/15/developing-spring-boot-applications-with-kotlin](https://spring.io/blog/2016/02/15/developing-spring-boot-applications-with-kotlin)).

It is a fun language to use and soon or later, most of the features in Kotlin will make it way into future version of Java.

The good news is that we can use Kotlin in our Java code today.
Add dependency to Kotlin Stdlib

```xml
<dependencies>
    <dependency>
        <groupId>org.jetbrains.kotlin</groupId>
        <artifactId>kotlin-stdlib</artifactId>
        <version>1.2.10</version>
    </dependency>
</dependencies>
```

You can find the latest version of kotlin-stdlib in [https://mvnrepository.com/artifact/org.jetbrains.kotlin/kotlin-stdlib](https://mvnrepository.com/artifact/org.jetbrains.kotlin/kotlin-stdlib)

 
### Writing Kotlin Code

There are many features of Kotlin that right away useful to remove boiler plates code from our Java project. See: [https://kotlinlang.org/docs/reference/comparison-to-java.html](https://kotlinlang.org/docs/reference/comparison-to-java.html)

One of them is __Data Class__. Example:

a. Add a new file PartNumber.kt (kt is extension for Kotlin source file)

b. Add a data class PartNumberFormat
```kotlin
    //this is the whole content of PartNumber.kt
     
    package au.infomedia.data
     
    // Kotlin's Data Class provide setter, getter, toString, compareTo/hashCode that we have to code manually/generated in Java
    data class PartNumberFormat(val prefix: String, val number: String, val suffix: String)
```

c. In our Java code, we can use PartNumberFormat straight away (Thanks to the magic of IntelliJ, no compile or configuration is required)

```java
    import au.infomedia.data.PartNumberFormat;
     
    ...
       PartNumberFormat partNumberFormat = new PartNumberFormat("P", number, suffix);
       ...
       String partNumberSuffix = partNumberFormat.getSuffix()
```

d. Data class give you getter, setter, toString, hashCode/equals, and copy without have to explicitly writing them

e. On top of that, you can also specify default value for the parameter

```kotlin
    //this is the whole content of PartNumber.kt
     
    package au.infomedia.data
     
    data class PartNumberFormat(val prefix: String, val number: String, val suffix: String)
     
    data class PartNumberFormatWithDefault @JvmOverloads constructor(val prefix: String, val number: String, val suffix: String = "AA")

```

```java
    import au.infomedia.data.PartNumberFormatWithDefault;
     
    ...
       PartNumberFormatWithDefault partNumberFormat = new PartNumberFormatWithDefault("P", number);
       ...
       String partNumberSuffix = partNumberFormat.getSuffix();      // return "AA"
```
