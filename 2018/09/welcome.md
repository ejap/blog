# 10/09/2018

## Introduction

Java is evolving fast and Java today is quite different from Java 5 years ago.

I remember most of the talk about Java 5 years ago was about how it was as a 
language so far behind .NET in term of the language features.

JVM has an advantage due to its open specifications and mistrust of Microsoft 
and its licensing model (I think - I am not an expert)

Back to the Java language.

When Java 7 was released, one of the thing I remember was the mild excitement 
that our Java code will be less verbose.

This was when Ruby and Python were all the craze and becoming more popular 
for good reason. Developer want to write elegant code, and Java doesn't come 
to mind at that time when writing beautiful code. There were just too many 
plumbing code needed.

A combination of Java 7, Google's Java Libraries (Guava, Guice, and a few others), 
and excellent IDE such as IntelliJ make coding in Java fun again.

In the next entry, I will write more about my recollection of the tipping point 
at that time.

There will be also many old articles that I post somewhere else (mainly on 
Intranet) that I find interesting to share.

Regards, ...
