# 27/09/2018

## AWS Lambda - Getting Started

In AWS world, Lambda is service where developer can define functions
that can be triggered by events from various AWS services.

The most common event source are:
- S3 (e.g. when a new file is uploaded)
- SQS (e.g. when a new message is available in the queue)
- DynamoDB (e.g. when a new item is added)

The complete list of event source can be found here: 
[https://docs.aws.amazon.com/lambda/latest/dg/invoking-lambda-function.html](https://docs.aws.amazon.com/lambda/latest/dg/invoking-lambda-function.html)

Lambda is also becoming more popular as web application backend which
commonly referred to as web services. The event source for this is
via AWS API Gateway.

Lambda functions can be written in various languages and it appears
that Node.JS and Python is the most widely supported language. It also
have a good support for Java 8.

A small and simple Lambda function can be uploaded and tested on AWS
itself, but for larger functions where you might want to test and
debug in your local development environment.

For local development, there are a few alternatives:

#### AWS SAM CLI

More details can be found here: 
- [https://docs.aws.amazon.com/lambda/latest/dg/test-sam-cli.html](https://docs.aws.amazon.com/lambda/latest/dg/test-sam-cli.html)
- [https://github.com/awslabs/aws-sam-cli](https://github.com/awslabs/aws-sam-cli)

#### aws-serverless-java-container

More details can be found here: 
- [https://github.com/awslabs/aws-serverless-java-container](https://github.com/awslabs/aws-serverless-java-container)
- [https://aws.amazon.com/blogs/opensource/java-apis-aws-lambda/](https://aws.amazon.com/blogs/opensource/java-apis-aws-lambda/)

#### Serverless Framework

- This is a platform agnostic framework (support AWS, Azure, GCP, etc)
but unfortunately doesn't have a good support for Java (as far as I 
can tell from the sample code)
- [https://serverless.com/](https://serverless.com/) 

### Additional Services Required

As with many other AWS services, these are the core services that are
required to use AWS Lambda:
- IAM (to define permissions)
- CloudWatch (logging)
- S3 (to upload your function, especially those written in Java/*.jar)

### Competitors

__Microsoft__ and __Google__ have similar offerings called __Microsoft Azure
Functions__ and __Google Cloud Functions__ respectively.
