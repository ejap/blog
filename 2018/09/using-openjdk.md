# 11/09/2018

## Using OpenJDK

It is only natural to start using OpenJDK on development machine since 
all our applications are deployed on AWS and using OpenJDK.

### 1. Download OpenJDK

I am using Windows 10 and in recent time, there are more vendors that
provide OpenJDK binaries. The first stop is 
[https://adoptopenjdk.net](https://adoptopenjdk.net) and select the
preferred binaries to download. I noticed that the binary size (73 Mb for
OpenJDK 8 with Hotspot is much smaller that Oracle JDK which is about
double that)

### 2. Uninstall Other JDKs

On Windows, this is done easily via "Add or Remove Programs" in Control
Panel.

### 3. Install OpenJDK

There are no installer, so just extract the OpenJDK zip file to your bin
or Program Files directory. I normally extract it to C:\bin\ or D:\bin\

### 4. Update Environment Variables

The 2 environment variables to update are __JAVA_HOME__ and __PATH__

![JAVA_HOME](assets_using-openjdk/environment-1.png "JAVA_HOME")

![PATH](assets_using-openjdk/environment-2.png "PATH")

### 5. Registry Change

This is required if you want to use JRE that come with your OpenJDK.

You can skip this if you use Oracle JDK or find that modifying Registry
entry is too daunting.

Not many applications nowadays use JRE. Mostly are Java development tools
such as as SonarQube or JMeter.

![Registry Change #1](assets_using-openjdk/registry-update-1.png "Registry Change #1")

![Registry Change #2](assets_using-openjdk/registry-update-2.png "Registry Change #2")

Failure to do so will give you errors similar to this:

![Sonar Error](assets_using-openjdk/sonar-error.png "Sonar Error")

## Roadblock (will keep up to date)

### 20/09/2018

A few days into using OpenJDK, I encountered this error when running one
of our application:

```
2018-09-20 11:28:29,163 [AmazonHttpClient] INFO  - Unable to execute HTTP request: java.lang.RuntimeException: Unexpected error: java.security.InvalidAlgorithmParameterException: the trustAnchors parameter must be non-empty
javax.net.ssl.SSLException: java.lang.RuntimeException: Unexpected error: java.security.InvalidAlgorithmParameterException: the trustAnchors parameter must be non-empty
    at sun.security.ssl.Alerts.getSSLException(Alerts.java:208)
```

A bit of googling found that OpenJDK doesn't ship with a valid/complete
 __cacerts__ file. Which from what I gather is "is a collection of 
trusted certificate authority (CA) certificates.". Hence the error
when trying to make https/ssl connection.

The quick and easy way to fix way to fix this is to install a matching
Oracle's JDK and look for the file __cacerts__ (in subdirectory
__/jre/lib/security__) and copy it to the same location in your OpenJDK
install (__/jre/lib/security__)
