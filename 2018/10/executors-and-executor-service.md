# 31/10/2018

## Using Executors and ExecutorService

### Creating ExecutorService

ExecutorService = Thread Pool

From Javadoc: An Executor that provides methods to __manage termination__ and methods that __can produce a Future__ for tracking progress of one or more __asynchronous tasks__.

| Methods to create ExecutorService | Description from JavaDoc |
| --------------------------------- | ------------------------ |
| _newFixedThreadPool(nThreads)_ | Creates a thread pool that __reuses a fixed number of threads__ operating off a __shared unbounded queue__. At any point, at most __nThreads__ threads will be active processing tasks. If additional tasks are submitted when all threads are active, they will __wait in the queue__ until a thread is available.
| |
| _newSingleThreadExecutor()_ | Creates an Executor that uses a __single worker thread__ operating off __an unbounded queue__. Tasks are __guaranteed to execute sequentially__, and no more than one task will be active at any given time.
| |
| _newWorkStealingPool()_ | Creates a thread pool that maintains __enough threads__ to support the given parallelism level, and may use multiple queues to reduce contention. The actual number of threads may __grow and shrink dynamically__. A work-stealing pool makes __no guarantees about the order__ in which submitted tasks are executed. |
| |
| _newCachedThreadPool()_ | Creates a thread pool that creates new threads __as needed__, but will reuse previously constructed threads when they are available. ... will typically improve the performance of programs that execute many __short-lived asynchronous tasks__. Threads that have not been used for __sixty seconds__ are terminated and removed from the cache. |
| |
| _newSingleThreadScheduledExecutor()_ | Creates a single-threaded executor that can __schedule__ commands to __run after a given delay__, or to __execute periodically__. Tasks are guaranteed to execute __sequentially__, and no more than __one task__ will be active at __any given time__. |
| |
| _newScheduledThreadPool(corePoolSize)_ | Creates a thread pool that can __schedule__ commands to __run after a given delay__, or to __execute periodically__. _corePoolSize_ = the number of threads to keep in the pool, even if they are idle |
| |
	




### ThreadFactory

ThreadFactory basically a factory that call __new Thread(...)__ with other configuration added. One of the use is to name the threads that created by one of the methods above.

Guava provide a handy builder class ThreadFactoryBuilder:

```java
ThreadFactory threadFactory = new ThreadFactoryBuilder()
    .setNameFormat("ssmv1-calls-thread-%d")
    .build();
ExecutorService executorService = Executors.newFixedThreadPool(nThreads, threadFactory);
```
 
### Thread, Runnable, and Callable

- Runnable __interface__ should be implemented by any class whose instances are intended to be executed by a thread
- Callable __interface__ is Runnable that returns a result and may throw an exception
- Thread class implements Runnable interface and is a thread of execution in a program with features such as:
    - set priority
    - can have name
    - can be put to sleep
    - etc

#### Runnable

```java
//1.
 Runnable runnable = new Runnable() {
     @Override
     public void run() {
        //some runnable task here
     }
 };
 
 
 //2.
 Runnable runnable = () -> {
    //some runnable task here
 };
 
 
 //3.
 class DatapakRunnable implements Runnable {
    @Override
    public void run() {
       //some runnable task here
    }
 }
```

 
#### Callable

```java
//1.
Callable<String> callable = new Callable<String>() {
    @Override
    public String call() throws Exception {
       //some callable task here
       return string;
    }
};
 
//2.
Callable<String> callable = () -> {
   //some callable task here
   return string;
};
 
//3.
class DatapakCallable implements Callable<String> {
   @Override
   public String call() {
      //some runnable task here
      return string;
   }
}
```
 
#### Thread

```java
//1. Alternative 1
 Thread thread = new Thread() {
     @Override
     public void run() {
         //some thread tasks here
     }
 };
 thread.setName("datapak-thread-" + counter);
 thread.setPriority(1);
 
 
 //2. Alternative 2 -
 Thread thread = new DatapakThread();
  
 /*
 class DatapakThread extends Thread {
    public DatapakThread() {
       super.setName(...);
       super.setPriority(1);
    }
 
    @Override
    public void run() {
       //some thread tasks here
    }
 }
 */
 
 //3. Alternative 3 - using ThreadFactory
 ThreadFactory threadFactory = new ThreadFactoryBuilder()
         .setNameFormat("datapak-thread-%d")
         .setPriority(1)
         .build();
 thread = threadFactory.newThread(() -> {
     //some thread task here
 });
 
 
 
 //use executorService to execute thread
 Executors.newSingleThreadExecutor().submit(thread);
 
 //you can also do thread.start() and it will run the thread's run() method but it is not commonly done this way.
```
 
### Future and CompletableFuture

- Future __interface__ represents the result of an asynchronous computation.
- CompletableFuture __class__ is a Future that may be explicitly completed (setting its value and status)

 
#### Future

```java
Future<String> future = executorService.submit(() -> {   //this callable will be executed as soon as executorService has resource available. submit() will not block.
   //some callable task
);
 
//some other task. future above is running in another thread as managed by executorService
...
 
//get the result of the future
future.get();                         //future may or may not completed. If not completed, this line will block
// future.get(5, TimeUnit.SECONDS);   //future may or may not completed. If not completed, this line will block for 5 SECONDS
```
 
#### CompletableFuture

CompletableFuture is a big class (2,300+ lines) and provides many features to with Futures

One of the nice feature is chaining of work so the resulting code is concise (similar to writing code with Java Stream)

It also provide __async__ methods (on top of the sync one) so the work will be done asynchronously.

Async methods will have suffix Async (i.e. thenApplyAsync(), thenAcceptAsync(), thenCombineAsync(), thenAcceptBothAsync(), thenComposeAsync(), ...)

```java
// - .supplyAsync(supplier) is the most common way to "start" the CompletableFuture chain.
// - There is no .supply(..) without Async suffix (a synchronous version)
// - .thenApply() is the most common way to do something with the input and pass to the next chain
// - .thenApply() is similar to Stream's .map(). There are 2 other similar method (.thenCombine() and .thenCompose()) with examples below:
 
CompleteableFuture<Order> future = CompletableFuture.supplyAsync(() -> getOrder(orderId))
    .thenApply(order -> processVehicle(order))
    .thenApply(order -> processOperation(order))
    .thenApply(order -> processItems(order));
return future.get(10, TimeUnit.SECONDS);
```
 
##### .thenApplyAsync()
```java
CompleteableFuture<Order> future = CompletableFuture.supplyAsync(() -> getOrder(orderId));
 
//some blocking code that must be done here
...
 
future = future.thenApplyAsync(order -> processOperation(order));    // thenApplyAsync() also accept executorService as 2nd parameter   
 
 
return future.get(10, TimeUnit.SECONDS);    //NOTE: will wait for 10 SECONDS for the future to be completed
```
 
##### .thenApply() and .thenCombine()
```java
CompleteableFuture<Order> future = CompletableFuture.supplyAsync(() -> getOrder(orderId))
    .thenApply(order -> {
        Vehicle vehicle = getVehicle(vin);
        return processVehicle(order, vehicle);
    })
    .thenApply(order -> processOperation(order))
    .thenApply(order -> processItems(order));
 
// can be rewritten as:
CompleteableFuture<Order> future = CompletableFuture.supplyAsync(() -> getOrder(orderId))
    .thenCombine(CompletableFuture.supplyAsync(() -> getVehicle(vin)), (order, vehicle) -> processVehicle(order, vehicle))
    .thenApply(order -> processOperation(order))
    .thenApply(order -> processItems(order));
```

##### .thenApply() and .thenCompose()
```java
CompleteableFuture<Order> future = CompletableFuture.supplyAsync(() -> getOrder(orderId))
    .thenApply(order -> processVehicle(order))
    ...
 
// can also be rewritten as:
CompleteableFuture<Order> future = CompletableFuture.supplyAsync(() -> getOrder(orderId))
    .thenCompose(order -> CompletableFuture.supplyAsync(() -> processVehicle(order)))
    ...
 
/*
   .thenApply() and .thenCompose() both are doing the same thing.
   .thenApply() accepts Function<T, U> and .thenCompose() accepts Function<T, CompletionStage<>>.
   Just think of .thenApply() as .map() equivalent and .thenCompose() as .flatMap() equivalent.
*/
```

#### Exception Handling
```java
CompleteableFuture<Order> future = CompletableFuture.supplyAsync(() -> getOrder(orderId))
    .thenApply(order -> processVehicle(order))
    .thenApply(order -> processOperationThatMightThrowException(order))
    .thenApply(order -> processItems(order))
    .handle((order, throwable) -> {
    // - any of the chain above that throw exception will go straight here
    // - also always go into .handle() so we need (throwable != null) check

    if (throwable != null) {
        log.error("...", throwable);
        return newEmptyOrder();   //order will be null if throwable is not null
    }
    else {
        return order;
    }
});
```
 
### Using ExecutorService

```java
ExecutorService executorService = Executors.newFixedThreadPool(10);
 
//1
result = executorService.submit(thread or callable or runnable ...)
                        .get(timeout, TimeUnit.SECONDS);
 
 
//2 - from Javadoc: Executes the given tasks, returning a list of Futures holding their status and results when all complete.
List<T> results = executorService.invokeAll(listOfCallables, timeout, TimeUnit.SECONDS)
    .stream()
    .map(future -> future.get(...))
    //optional
    //.filter(...)
    //.map(...)
    .collect(...);
 
//3 - from Javadoc: Executes the give tasks, returning the result of one that has completed successfully
T result = executorService.invokeAny(listOfCallables)
    .stream()
    .orElse(...)
```
 
### Guava Classes

- Beside ThreadFactoryBuilder mentioned above, there are many classes in Guava that helps to deal with concurrency and parallelism
- Some examples:
    - https://github.com/google/guava/wiki/ListenableFutureExplained
    - https://github.com/google/guava/wiki/ServiceExplained