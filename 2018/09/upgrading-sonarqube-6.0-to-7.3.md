# 17/09/2018

## Upgrading SonarQube 6.0 to 7.3

### 1. Download the binaries

Go to [https://www.sonarqube.org/downloads/](https://www.sonarqube.org/downloads/) 
and download latest 6.x version (reason see __item 3__ below) 
sonarqube-6.7.5.zip and 7.3 (or latest version) sonarqube-7.3.zip

Once downloaded, scp the zip files to the server.

```
$ scp sonarqube-6.7.5.zip sonarqube.ejap.id:~

$ scp sonarqube-7.3.zip sonarqube.ejap.id:~
```

Once copied, unzip them

```
$ unzip sonarqube-6.7.5.zip
 
$ unzip sonarqube-7.3.zip
 
$ ll
drwxr-xr-x 11 ejap INTDEV_SUDO      4096 Aug  6 10:29 sonarqube-6.7.5
-rwxr-xr-x  1 ejap INTDEV_SUDO 159614083 Sep 11 17:21 sonarqube-6.7.5.zip
-rw-r--r--  1 ejap INTDEV_SUDO 150092853 Apr 17 19:17 sonarqube-7.1.zip
drwxr-xr-x 11 ejap INTDEV_SUDO      4096 Aug 10 17:40 sonarqube-7.3
 
$ ll sonarqube-6.7.5
total 44
drwxr-xr-x 8 ejap INTDEV_SUDO 4096 Aug  6 10:29 bin
drwxr-xr-x 2 ejap INTDEV_SUDO 4096 Aug  6 10:20 conf
-rw-r--r-- 1 ejap INTDEV_SUDO 7651 Aug  6 10:20 COPYING
drwxr-xr-x 4 ejap INTDEV_SUDO 4096 Sep 11 17:38 data
drwxr-xr-x 7 ejap INTDEV_SUDO 4096 Aug  6 10:29 elasticsearch
drwxr-xr-x 5 ejap INTDEV_SUDO 4096 Sep 11 17:28 extensions
drwxr-xr-x 9 ejap INTDEV_SUDO 4096 Aug  6 10:29 lib
drwxr-xr-x 2 ejap INTDEV_SUDO 4096 Sep 11 17:28 logs
drwxr-xr-x 6 ejap INTDEV_SUDO 4096 Sep 11 17:38 temp
drwxr-xr-x 9 ejap INTDEV_SUDO 4096 Aug  6 10:29 web
```

### 2. Cleanup current version

Stop the service if running

```
$ service sonar status
SonarQube is running (3105).
 
$ service sonar stop
```
 
Unregister sonar service

```
$ chkconfig --list | grep sonar
sonar           0:off   1:off   2:off   3:on    4:on    5:on    6:off

 
$ chkconfig --del sonar
 
 
$ chkconfig --list | grep sonar
 
$
```
 

Delete (backup) the files / directory:
- /etc/init.d/sonar
- /opt/sonar

### 3. Create a user to run SonarQube

Learning this the hard way. So after upgrading to later version, 
SonarQube won't start because the search engine (powered by 
ElasticSearch) won't run with root user anymore. I can reuse the 
existing account but decided to create a new user.

```
org.elasticsearch.bootstrap.StartupException: java.lang.RuntimeException: can not run elasticsearch as root
```
 
Creating new user (sonaruser)

```
$ useradd sonaruser
...
 
$ passwd sonaruser
Changing password for user sonaruser.
New password: ...
 
// add sonaruser as sudoers
$ visudo
 
// add sonaruser to the end of the sudoers file
```
 
### 4. Install latest 6.x version

I was also learning this the hard way.

Trying to install SonarQube 7.3 and was told that since we were 
running 6.0, I must upgrade to latest 6.x version, upgrade the 
database, and then upgrade to 7.x

```
Web server startup failed: Current version is too old. Please upgrade to Long Term Support version firstly.
```

a. switch to user __sonaruser__

```
$ su sonaruser
...
```
 
b. Copy unzipped directory __sonarqube-6.7.5__ to __/opt/sonarqube-6.7.5__

```
$ cp sonarqube-6.7.5 /opt
```
 
c. Update __/opt/sonarqube-6.7.5/conf/sonar.properties__

```
sonar.jdbc.username=sonar-user
sonar.jdbc.password=...
 
sonar.jdbc.url=jdbc:mysql://localhost:3306/sonar?useUnicode=true&characterEncoding=utf8&rewriteBatchedStatements=true&useConfigs=maxPerformance
 
# TCP port for incoming HTTP connections. Default value is 9000.
# NOTE: httpd is configured to proxy to port 8080
sonar.web.port=8080
 
# NOTE: change this to a valid path or create a symbolic link /opt/sonar -> /opt/sonarqube-6.7.5)
sonar.path.logs=/opt/sonar/logs


# NOTE: if SonarQube won't start, enable the DEBUG logging level to get more information
sonar.log.level.app=DEBUG
sonar.log.level.web=DEBUG
sonar.log.level.ce=DEBUG
sonar.log.level.es=DEBUG
```
 
d.  Run it in __console__ mode

```
$ cd /opt/sonarqube-6.7.5/bin/linux-x86-64
 
$ ./sonar.sh console
```
 
e. You will get this error

```
Establishing SSL connection without server's identity verification is not recommended. According to MySQL 5.5.45+, 5.6.26+ and 5.7.6+ requirements SSL connection must be established by default if explicit option isn't set.
 
For compliance with existing applications not using SSL the verifyServerCertificate property is set to 'false'. You need either to explicitly disable SSL by setting useSSL=false, or set useSSL=true and provide truststore for server certificate verification.
```

At this time, I decided to just disable SSL since MySQL is running on 
the same machine. Edit this line in sonar.properties to add __&userSSL=false__ 
to MySQL connection string

```
# BEFORE:
#sonar.jdbc.url=jdbc:mysql://localhost:3306/sonar?useUnicode=true&characterEncoding=utf8&rewriteBatchedStatements=true&useConfigs=maxPerformance
 
# AFTER:
sonar.jdbc.url=jdbc:mysql://localhost:3306/sonar?useUnicode=true&characterEncoding=utf8&rewriteBatchedStatements=true&useConfigs=maxPerformance&userSSL=false
```
 
f. Rerun again

```
$ ./sonar.sh console
 
// check the log
$ tail -f /opt/sonarqube-6.7.5/logs/sonar.log
```
 
g. Open SonarQube in browser at address [http://sonarqube.ejap.id]. 
If all goes well, it will ask to upgrade the database by clicking this 
link [http://sonarqube.ejap.id/setup]

 
### 5. Cleanup After Upgrade to latest 6.x version

a. Type __Ctrl+C__ to stop SonarQube

b. Delete the directory

- /opt/sonarqube-6.7.5

### 6. Install version 7.3

This step is identical with step __4. Install latest 6.x version__

### 7. Install as service

Once SonarQube 7.3 is running (in __console__ mode), we need to install 
it as a service so it will start when the machine is restarted

The instruction can be found here: [https://docs.sonarqube.org/display/SONAR/Running+SonarQube+as+a+Service+on+Linux](https://docs.sonarqube.org/display/SONAR/Running+SonarQube+as+a+Service+on+Linux)

Below are the instructions in order:

a. Create a symbolic link of __sonar.sh__ in __/usr/bin__ directory

```
$ ln -s /opt/sonarqube-7.3/bin/linux-x86-64/sonar.sh /usr/bin/sonar
```
 
b. Configure the "Run As" user (NOTE: remember that SonarQube won't start as root). 
Edit file sonar.sh linked above and add:

```
RUN_AS_USER=sonaruser
```
 
c. Create the file __/etc/init.d/sonar__ with this content:

```
#!/bin/sh
#
# rc file for SonarQube
#
# chkconfig: 345 96 10
# description: SonarQube system (www.sonarsource.org)
#
### BEGIN INIT INFO
# Provides: sonar
# Required-Start: $network
# Required-Stop: $network
# Default-Start: 3 4 5
# Default-Stop: 0 1 2 6
# Short-Description: SonarQube system (www.sonarsource.org)
# Description: SonarQube system (www.sonarsource.org)
### END INIT INFO
  
/usr/bin/sonar $*
```
 
d. Set the correct permission and register the service

```
$ chmod 755 /etc/init.d/sonar
 
$ chkconfig --add sonar
```
 
e. Start the service

```
$ service sonar start
```
 
### 9. Restart the box

If SonarQube is running successfully ([http://sonarqube.ejap.id]), 
restart the machine to make sure SonarQube will be fine in the event 
of machine reboot, etc

```
$ shutdown -r now
```
