# Erfan's Blog

## September 2018

- [Welcome](2018/09/welcome.md)
- [Tools: Using OpenJDK](2018/09/using-openjdk.md)
- [Browser Notification](2018/09/browser-notification.md)
- [Basic: Polymorphism](2018/09/polymorphism.md)
- [Basic: Bitwise Operation](2018/09/bitwise-operation.md)
- [Tools: Upgrading SonarQube 6.0 to 7.3](2018/09/upgrading-sonarqube-6.0-to-7.3.md)
- [Basic: Sorting](2018/09/sorting.md)
- [Code Review Series: Try With Resources](2018/09/try-with-resources.md)
- [Conference: Google Cloud Platform - Sydney](2018/09/gcp-conference-2018.md)
- [AWS Lambda: Getting Started](2018/09/lambda-getting-started.md)
- [AWS Lambda: Handling S3 Event - step by step](2018/09/lamba-with-s3-step-by-step.md)

## October 2018

- [Learning Kotlin - Part 1](2018/10/learn-kotlin-1.md)
- [Learning Kotlin - Part 2](2018/10/learn-kotlin-2.md)
- [Upgrading to JUnit 5 - Part 1](2018/10/upgrading-to-junit5-1.md)
- [Upgrading to JUnit 5 - Part 2](2018/10/upgrading-to-junit5-2.md)
- [Using Executors and ExecutorService](2018/10/executors-and-executor-service.md)


## Reference

- Blog address is 
[https://bitbucket.org/ejap/blog/src/master/readme.md](https://bitbucket.org/ejap/blog/src/master/readme.md)
