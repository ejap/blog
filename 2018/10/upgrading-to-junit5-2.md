# 26/10/2018

## Upgrading to JUnit 5 - Part 2

### Assertions Class

a. Assertion methods are available from class org.junit.jupiter.api.Assertions. Check out the __javadoc__ ([http://junit.org/junit5/docs/current/api/](http://junit.org/junit5/docs/current/api/)) and __user guide__ ([http://junit.org/junit5/docs/current/user-guide/#writing-tests-assertions](http://junit.org/junit5/docs/current/user-guide/#writing-tests-assertions)) of all the assertion methods provided. 

b. Assertion message now can be lazily evaluated. They will be evaluated only if assertion failed. This is useful if your assertion message call methods that take time to be evaluated. This is similar to lazy logging introduced by _Log4J 2_. If assertion is valid, the assert message will never get called.

```java
Assertions.assertEquals(5, person.getSubscriptionCount(), () -> "subscription for [" + personLogic.getFilteredSubNameAsString() + "] should be excluded");
```

c. Assertions can be grouped. This makes the assertion better organized. See also nested test class below.

```java
import org.junit.jupiter.api.Assertions;
...
assertAll("check valid person's subscription and company",
   () -> assertEquals(5, person.getSubscriptionCount()),
   () -> assertEquals(2, person.getCompanyCount())
);
```

d. Assertion for exceptions. Previously in _JUnit 4_, exception check only allowed on the method level.

```java
//JUnit4
@Test(expected=AuthorizationException.class)
void testAuthenticateChecksheetWithInvalidUser {
   ...
}
 
 
//JUnit5
import org.junit.jupiter.api.Assertions;
...
//assert success if code block throw AuthorizationException exception
//if not exception thrown, it will fail with "AssertionFailedError: Expected AuthorizationException to be thrown, but nothing was thrown."
assertThrows(AuthorizationException.class, () ->
   builder = healthCheckSettingLogic.authenticateChecksheet(checksheet, notValidUser);
);
```

e. Assertion for timeout. I don't see any use case for this yet as most of the time consuming activity is I/O related and we usually mock the I/O classes.

```java
import org.junit.jupiter.api.Assertions;
...
// if the code block took longer than 500ms, it will fail the test (AssertionFailedError: execution exceeded timeout of 500 ms by ... ms)
assertTimeout(ofMillis(500), () -> {
   personLogic.someTimeConsumingMethod();         
});
```

### Disabling Test

a. The first option to disabling tests is by using the _@Disabled_ annotation. It can be placed on a class level or on a method. This is usually meant for marking an obsolete tests or a temporary solution due to test failure. This is similar to _@Ignore_ from _JUnit 4_

```java
@Disabled
public class PersonLogicTest {
 
//or
 
public class PersonLogicTest {
 
    @Disabled
    public void testGetPreferredCompany() {
``` 

b. Option #2 is to use assumptions (i.e. org.junit.jupiter.api.Assumptions.__assumeTrue__, org.junit.jupiter.api.Assumptions.__assumingThat__, etc). Similar assumption methods also available in _JUnit 4_. _JUnit 5_ support lambda/Supplier.

```java
public void testGetPreferredCompany() {
 
    boolean testMultiSite = DynamicPropertyFactory.getInstance().getBooleanProperty("testMultiSite").getValue();
 
    assumeTrue(testMultiSite);  // exit method if testMultiSite is false
 
    //the rest of test here ...
```

c. Option #3 is to use tagging. You can tag a test class or a test method and you can filter out the test to run based on the tag

```java
@Tag("important")
public class PersonLogicTest {
 
    @Tag("multi-site")
    public void testGetPreferredCompany() {
```

- NOTE: IntelliJ not yet support test filtering by tag
- In Maven, you can set the property _excludeTags_ and _includeTags_ for the surefire plugin:

```xml
<plugin>
    <artifactId>maven-surefire-plugin</artifactId>
    ...
    <configuration>
        <properties>
            <excludeTags>multi-site</excludeTags>
        </properties>
    </configuration>
</plugin>
```

### Nested Test
A nicer way to organize your test. Instead of creating separate test class in separate files.

```java
class PersonLogicTest {
 
    @Nested
    class CompanyRelatedTest {
 
        Person person;
        @BeforeEach
        void setup() {
            person = PersonFactory.getPersonForCompanyTest();
        } 
 
        @Test
        void testGetPersonCompany() {
 
 
    @Nested
    class SubscriptionRelatedTest {
 
        Person person;
        @BeforeEach
        void setup() {
            person = PersonFactory.getPersonForSubscriptionTest();
        } 
```

### Parameterised Test
Another useful feature that fall into "making test less verbose" category is parameterized test. Example:

```java
@ParameterizedTest
@ValueSource(strings = {VALID_EMAIL, INVALID_EMAIL})
void testGetPersonReferenceErrorHandling(String username) {
    assertNotNull(personLogic.getReferenceByUsername(username));
}
```

The test above will be run multiple times using the values in _@ValueSource_ annotations that is passed as an argument to the test method.

To use the annotation _@ParameterizedTest_ and _@ValueSource_, we need to add the following library in _pom.xml_

```xml
<dependency>
    <groupId>org.junit.jupiter</groupId>
    <artifactId>junit-jupiter-params</artifactId>
    <version>5.0.0-M4</version>
    <scope>test</scope>
</dependency>
```

### Mockito Extension

Note that Mockito extension is still in experimental phase so expect changes in the future. But it is quite an elegant way to use Mockito compared to how it is in _JUnit 4_.

```java
@ExtendWith(MockitoExtension.class)
class PersonLogicTest {
     
    CompanyLogic companyLogic;
 
    @BeforeEach
    void setup(@Mock CompanyLogic companyLogic) {
        when(companyLogic.getProviderId()).thenReturn(VALID_PROVIDER_ID);
    } 
     
    @Test
    void testGetPersonCompany(@Mock CompanyLogic companyLogic) {
         
    }
}
```
