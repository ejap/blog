# 14/09/2018

## Polymorphism

Polymorphism is an ability of an object in Java to be polymorphic, which
from JDK documentation notes that it is a term in Biology in which
an organism that can take many forms.

Now we get the description out of the way, if asked in 10 words of less
what is polymorphism. My answer will be

> An object A that extends base class B and implements interface C
is polymorphic because it can be assigned to objects of type B or C
and can be passed to methods that accepts objects of type B or C.

So that is longer that 10 words. :-)

Polymorphism is very useful for library developers because they can
write code that work on base class or interface and doesn't have
to worry that it won't be extensible.

Polymorphism also make writing unit test easier because combined
with dependency injection we can write test code that doesn't have
deal with actual resources (database, etc).

### References

- [https://docs.oracle.com/javase/tutorial/java/IandI/polymorphism.html](https://docs.oracle.com/javase/tutorial/java/IandI/polymorphism.html)
