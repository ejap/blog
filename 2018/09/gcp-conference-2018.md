# 26/09/2018

## Conference: Google Cloud Platform - Sydney

I attended the Google Cloud Platform and learn a few new and
interesting up-and-coming technologies.

### DevOps Related

Since I am not a DevOps (even though in this time and age all
developer should), some of the new technology that was
presented just flew over my head.

But there are a few cool demos that worth mentioning:

- [Spinnaker](https://www.spinnaker.io/), a continuous delivery
platform that seems to be the way forward in the future.
I am a Bamboo user and quite happy with it but Spinnaker is
definitely a tool to watch for.

- [Istio](https://istio.io/), this is definitely something that
I didn't understand. There is a new concept called "Service Mesh"
which is something to read and learn about.

### UI

- [Cypress](https://www.cypress.io/). Coincidentally, a few days
before the conference I come across a tweet regarding a new
Cypress training on _egghead.io_. It is a new-ish frontend
end-to-end testing framework that looks good. Honestly 
end-to-end testing for AngularJS with Karma/Jasmine is hard.
When featured in a major conference like this, you know it is
going to be big.

This is in my __TODO/TO-LEARN__ list.

### Development

- [Apigee](https://apigee.com), an API Management Platform. From
the use cases that was presented, if you want inter-connectivity
between your organization entities or outside partner, you
should use Apigee or similar product to build and manage your
API.

- [Stackdriver](https://cloud.google.com/stackdriver/), this is
the demo that I most excited about. Stackdriver is actually a
suite of tools (I think they are called APM, Profiler,
and Debugger).
The demo on Debugger was very cool where breakpoints (that
won't slow down or stop the application) can be set and you 
can debug the Production application in the Stackdriver IDE.
This is a cool tool.
Of course you can debug remotely with Java but you must run
it in Debug mode and it is slow. It is not something that you
want to do in Production (where as Stackdriver claim you can
use to do Production Debugging with no side-effect)

All in all, it is good conference and it is always nice to learn
new cool tools for development.
